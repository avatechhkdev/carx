require('dotenv').config()
var bodyParser = require('body-parser')
const express = require('express');
const http = require("http");
const socketIO = require("socket.io");
const path = require("path");
const { format } = require('util');
const app = express();
app.use(bodyParser.json());

const Multer = require('multer');
const multer = Multer({
    storage: Multer.memoryStorage(),
    limits: {
        fileSize: 5 * 1024 * 1024, // no larger than 5mb.
    },
});

const redis = require('redis');
const REDISPORT = process.env.REDIS_PORT;

var redisClient = redis.createClient(REDISPORT, redis);
if (parseInt(process.env.PRODUCTION)) {
    const redisSvcIp = 'redis-service.default.svc.cluster.local'
    redisClient = redis.createClient(REDISPORT, redisSvcIp);
}
const subscriber = redisClient;
const publisher = redisClient;

const server = http.createServer(app);
const io = socketIO(server);
const port = process.env.PORT

const { Storage } = require('@google-cloud/storage');
const gc = new Storage({
    keyFilename: path.join(__dirname, process.env.GCP_STORAGE_KEY),
    projectId: process.env.GCP_PROJECT_ID
});

const terrainBucket = gc.bucket('carx-bucket')

const mongoose = require("mongoose");
mongoose
    .connect(
        `mongodb://mongodb-service/car`, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    })
    .then(() => console.log("MongoDB successfully connected40"))
    .catch(err => console.log(err));

app.get('/', (req, res) => {
    res.end(`Hi this is car app, PID: ${process.pid}`);
});

app.get('/health', (req, res) => {
    // your health check logic goes here
    res.status(200).send();
});

app.get('/ready', (req, res) => {
    // your readiness check logic goes here
    res.status(200).send();
});

app.post('/upload_file', multer.single('file'), (req, res, next) => {
    if (!req.file) {
        res.status(400).send('No file uploaded.');
        return;
    }
    const blob = terrainBucket.file(req.body.filename);
    const blobStream = blob.createWriteStream();

    blobStream.on('error', (err) => {
        next(err);
    });

    blobStream.on('finish', () => {
        // The public URL can be used to directly access the file via HTTP.
        const publicUrl = format(
            `https://storage.googleapis.com/carx-bucket/${blob.name}`
        );
        res.status(200).send(publicUrl);
    });

    blobStream.end(req.file.buffer);
});

app.post('/delete_file', function (req, res) {
    terrainBucket.file("mo2").delete();
    console.log("deleted")
});


io.on('connection', (socket) => {
    io.set('transports', ['websocket']);
    console.log(socket.id + " connected");

    socket.on("getServerInfo", () => {
        io.to(socket.id).emit("replyServerInfo", process.pid);
    })

    socket.on("regSub", () => {
        console.log(socket.id + " subscribed")
        io.to(socket.id).emit("replyFromServer", "ok, thanks for sub");

        subscriber.subscribe('my channel');
        subscriber.on('message', (channel, message) => {
            console.log(`Message "${message}" on channel "${channel}" arrived!`)
            io.to(socket.id).emit("publishFromServer", "someone published");
        });
    })

    socket.on("publishMsg", () => {
        console.log(socket.id + " publishMsg")
        publisher.publish('my channel', '123');
        publisher.publish('my channel', 'hello world');
    })

    socket.on("boardcastIO", () => {
        console.log(socket.id + " boardcastIO")
        socket.broadcast.emit('broadcast', 'hello friends!');
    })

    socket.on("disconnect", () => {
        console.log(socket.id + " is disconnected")
    })
});


console.log("50 Server running on " + port + " PID: " + process.pid);

server.listen(port, () => console.log(`Listening on port ${port}`));