import React, { Component } from 'react';
import socketIOClient from "socket.io-client";
import axios from 'axios';

var socket;
class SockPage extends Component {
    constructor() {
        super();
        this.state = {
            endpoint: process.env.REACT_APP_SERVER_API_URL,
            role: "",
            msg: "",
            serverInfo: "",
            status: "nothing",
            broadcastMsg: "no broadcast now",
            imageFile: null,
            imageChanged: false,
            uploadURL: process.env.REACT_APP_SERVER_API_URL + "/upload_file",
            deleteURL: process.env.REACT_APP_SERVER_API_URL + "/delete_file"
        };
        socket = socketIOClient(this.state.endpoint, { transports: ['websocket'], upgrade: false });
        console.log(socket)
    }

    componentDidMount() {
        socket.on("connect", () => {
            console.log("connected")
            this.setState({ status: "connected" })
        });
        socket.on("disconnect", () => {
            console.log("disconnected")
            this.setState({ status: "disconnected" })
        });
        socket.emit("getServerInfo");
        socket.on("replyServerInfo", (msg) => {
            this.setState({ serverInfo: msg })
        });
        socket.on("broadcast", (msg) => {
            this.setState({ broadcastMsg: msg })
        });
    }

    regSub = () => {
        socket.emit("regSub");
        this.setState({ role: "sub" })
        socket.on("replyFromServer", (msg) => {
            this.setState({ msg: msg })
        });
        socket.on("publishFromServer", (msg) => {
            this.setState({ msg: msg })
        });
    }

    publishMsg = () => {
        socket.emit("publishMsg");
        this.setState({ role: "pub" })
    }

    boardcastIO = (msg) => {
        socket.emit("boardcastIO");
    }

    onImageFileInputChange = (event) => {
        this.setState({ imageFile: event.target.files[0] });
        this.setState({ imageChanged: true });
    }

    uploadFile = (event) => {
        event.preventDefault()
        const formData = new FormData();
        formData.append('terrain_file', this.state.imageFile);
        console.log("upload_file")
        axios.post(process.env.REACT_APP_SERVER_API_URL + '/upload_file', formData, {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        }).then((response) => {
            if (response.data.err) {
                console.log(response.data.err)
            } else {
                this.setState({ imageFile: null });
            }

        }).catch((error) => {
            console.log(error)
        })
    }


    render() {
        return (
            <React.Fragment>
                Server info: {this.state.serverInfo}
                <h1>Client50</h1>
                {this.state.role}: {this.state.msg}
                <br></br>
                <button onClick={this.regSub}>
                    Sub
                </button> ~~~~~~

                <button onClick={this.publishMsg}>
                    publishMsg
                </button>

                <br>
                </br>
                Status: {this.state.status}
                <br />

                <button onClick={this.boardcastIO}>
                    boardcast with socket only
                </button>
                {this.state.broadcastMsg}

                <hr />
                <form action={this.state.uploadURL} method="post" enctype="multipart/form-data">
                    Select image to upload:
                    <input type="file" name="file" />
                    <input type="text" name="filename" />
                    <input type="submit" value="Upload Image" name="submit" />
                </form>

                <form action={this.state.deleteURL} method="post">
                    <input type="submit" value="Delete" name="submit" />
                </form>




            </React.Fragment>
        );
    }
}

export default SockPage;